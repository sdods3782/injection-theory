//
//  ModelTests.swift
//  ModelTests
//
//  Created by Sam Dods on 24/07/2020.
//

import XCTest
@testable import Model

class ModelTests: XCTestCase {
    
    let mockDate = DateFormatter.simple.date(from: "05/05/2020")!
    
    override func setUpWithError() throws {
        Container.resolver = MockResolver(currentDate: mockDate)
    }

    func testBasketManager() throws {
        let basketManager = BasketManager()
        let original = basketManager.basket
        XCTAssertTrue(original is MockBasket)
        (original as? MockBasket)?.id = UUID().uuidString
        let again = basketManager.basket
        XCTAssertEqual(original.id, again.id)
    }
    
    func testBasket() throws {
        let basket = Basket()
        XCTAssertEqual(basket.createdDate, mockDate)
        let product = Product(name: "test", id: "123")
        try basket.add(product)
        try basket.add(product)
        try basket.add(product)
        XCTAssertEqual(basket.totalNumberOfItems, 3)
        try basket.add(product)
        try basket.add(product)
        XCTAssertEqual(basket.totalNumberOfItems, 5)
    }
}

extension DateFormatter {
    static let simple: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        return df
    }()
}
