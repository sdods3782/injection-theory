//
//  Mocking.swift
//  ModelTests
//
//  Created by Sam Dods on 24/07/2020.
//

import Foundation
import Model

class MockBasket: BasketType {
    var id: String = ""
    var createdDate: Date {
        fatalError("Not needed for testing")
    }
    var totalNumberOfItems: Int = 0
    init() {}
    
    func add(_: Product) throws {
        totalNumberOfItems += 1
    }
}
class MockBasketManager: BasketManagerType {
    var basket: BasketType = MockBasket()
}

struct MockResolver: ModelDependencyResolver {
    func newBasket() -> BasketType {
        return MockBasket()
    }
    
    var currentDate: Date
    var basketManager: BasketManagerType = MockBasketManager()
}
