//
//  DependencyResolver.swift
//  Model
//
//  Created by Sam Dods on 24/07/2020.
//

import Foundation

public protocol ModelDependencyResolver {
    var basketManager: BasketManagerType { get }
    func newBasket() -> BasketType
    var currentDate: Date { get }
}

public enum Container {
    public static var resolver: ModelDependencyResolver!
}
