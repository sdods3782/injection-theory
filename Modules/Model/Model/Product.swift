//
//  Product.swift
//  Model
//
//  Created by Sam Dods on 30/07/2020.
//

import Foundation

public struct Product {
    public var name: String
    public var id: String
}
