//
//  Basket.swift
//  Model
//
//  Created by Sam Dods on 24/07/2020.
//

import Foundation

public protocol BasketType {
    var id: String { get }
    var totalNumberOfItems: Int { get }
    var createdDate: Date { get }
    func add(_: Product) throws
}

public class Basket: BasketType {
    public let id: String
    public var createdDate: Date = Container.resolver.currentDate
    private var products = [Product]()
    public var totalNumberOfItems: Int {
        return products.count
    }
    
    public init() {
        id = UUID().uuidString
    }
    
    public func add(_ product: Product) throws {
        products.append(product)
    }
}
