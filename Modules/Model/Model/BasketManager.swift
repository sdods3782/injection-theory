//
//  BasketManager.swift
//  Model
//
//  Created by Sam Dods on 24/07/2020.
//

import Foundation

public protocol BasketManagerType {
    var basket: BasketType { get }
}

public class BasketManager: BasketManagerType {
    private var current: BasketType?
    public var basket: BasketType {
        if let current = current {
            return current
        }
        let new = Container.resolver.newBasket()
        current = new
        return new
    }
    
    public init() {}
}
