//
//  AuthenticateTests.swift
//  AuthenticateTests
//
//  Created by Sam Dods on 30/07/2020.
//

import XCTest
@testable import Authenticate

class AuthenticateTests: XCTestCase {
    
    let mockNow = DateFormatter.simple.date(from: "30/07/2020")!
    let mockUser = User(name: "Sam", dateOfBirth: DateFormatter.simple.date(from: "06/06/1985")!)
    
    override func setUpWithError() throws {
        Container.resolver = MockResolver(session: MockSession.shouldSucceed(user: mockUser),
                                          currentDate: mockNow,
                                          calendar: Calendar(identifier: .gregorian))
    }

    func testSignIn() throws {
        let wait = expectation(description: "wait")
        UserManager().signIn(username: "test", password: "qwert123!") { result in
            guard case .success(let user) = result else {
                XCTFail()
                return
            }
            XCTAssertEqual(user.name, self.mockUser.name)
            XCTAssertEqual(user.age, self.mockUser.age)
            wait.fulfill()
        }
        waitForExpectations(timeout: 0)
    }

    func testUserAge() {
        XCTAssertEqual(mockUser.age, 35)
    }
}

extension DateFormatter {
    static let simple: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        return df
    }()
}
