//
//  Mocking.swift
//  AuthenticateTests
//
//  Created by Sam Dods on 30/07/2020.
//

import Foundation
import Authenticate

struct MockResolver: AuthenticateDependencyResolver {
    var session: DataFetching
    var currentDate: Date
    var calendar: Calendar
}

enum MockSession: DataFetching {
    case shouldSucceed(user: User)
    case shouldFail
    
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> ResumableTask {
        switch self {
        case .shouldSucceed(let user):
            let data = try! JSONEncoder().encode(user)
            completionHandler(data, nil, nil)
        case .shouldFail:
            completionHandler(nil, nil, nil)
        }
        return  MockTask()
    }
}

struct MockTask: ResumableTask {
    func resume() {}
}
