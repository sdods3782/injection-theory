//
//  User.swift
//  Authenticate
//
//  Created by Sam Dods on 30/07/2020.
//

import Foundation

public struct User: Codable {
    public var name: String
    public var dateOfBirth: Date
    
    public var age: Int {
        let now = Container.resolver.currentDate
        return Container.resolver.calendar.dateComponents([.year], from: dateOfBirth, to: now).year!
    }
}
