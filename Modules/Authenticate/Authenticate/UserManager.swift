//
//  UserManager.swift
//  Authenticate
//
//  Created by Sam Dods on 30/07/2020.
//

import Foundation

public class UserManager {
    
    struct SignInError: Error {}
    
    public func signIn(username: String, password: String, completion: @escaping (Result<User, Error>) -> Void) {
        Container.resolver.session.fetch(from: URL(string: "https://example.com/signin")!) { (data, response, error) in
            guard let data = data,
               let user = try? JSONDecoder().decode(User.self, from: data) else {
                completion(.failure(SignInError()))
                return
            }
            completion(.success(user))
        }
    }
}
