//
//  URLSessionType.swift
//  Authenticate
//
//  Created by Sam Dods on 30/07/2020.
//

import Foundation

public protocol DataFetching {
    func fetch(from: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void)
}
