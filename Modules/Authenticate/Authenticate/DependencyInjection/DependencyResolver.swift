//
//  DependencyResolver.swift
//  Authenticate
//
//  Created by Sam Dods on 30/07/2020.
//

import Foundation

public protocol AuthenticateDependencyResolver {
    var session: DataFetching { get }
    var currentDate: Date { get }
    var calendar: Calendar { get }
}

public enum Container {
    public static var resolver: AuthenticateDependencyResolver!
}
