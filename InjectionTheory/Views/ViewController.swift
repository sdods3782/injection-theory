//
//  ViewController.swift
//  InjectionTheory
//
//  Created by Sam Dods on 12/08/2020.
//

import UIKit
import Model

final class ViewController: UIViewController {
    
    @IBOutlet private var basketTotalLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basketTotalLabel.text = "\(Container.resolver.basketManager.basket.totalNumberOfItems)"
    }
}
