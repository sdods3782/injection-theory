//
//  DependencyResolver.swift
//  InjectionTheory
//
//  Created by Sam Dods on 24/07/2020.
//

import Foundation
import Model
import Authenticate

protocol DependencyResolver: ModelDependencyResolver, AuthenticateDependencyResolver {}

enum Container {
    static let resolver: DependencyResolver = DefaultResolver()
}

extension URLSession: DataFetching {
    public func fetch(from url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task: URLSessionDataTask = dataTask(with: url, completionHandler: completionHandler)
        task.resume()
    }
}

// MARK: - Resolution

private class DefaultResolver: DependencyResolver {
    
    var session: DataFetching {
        return URLSession.shared
    }
    
    var calendar: Calendar {
        return Calendar(identifier: .gregorian)
    }
    
    func newBasket() -> BasketType {
        return Basket()
    }
    
    var currentDate: Date {
        return Date()
    }
    
    let basketManager: BasketManagerType = BasketManager()
    
    init() {
        Model.Container.resolver = self
        Authenticate.Container.resolver = self
    }
}
