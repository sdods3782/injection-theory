//
//  AppDelegate.swift
//  InjectionTheory
//
//  Created by Sam Dods on 12/08/2020.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {}
